const http = require('http');
const postgres = require('postgres');
const pLimit = require('p-limit');

const sql = process.env.POSTGRES_URL ? postgres(process.env.POSTGRES_URL) : null;
const countRequests = 1000;

http.createServer(async function (req, res) {
  const start = performance.now();

  if (req.url.startsWith('/image')) {
    const response = await fetch('https://app.kontrool.de/assets/img/kontrool-logo.png');
    res.write(response.statusText);
  } else if (sql && req.url.startsWith('/postgres?concurrency=')) {
    const limit = pLimit(+req.url.replace('/postgres?concurrency=', ''));

    // Warmup
    const result = await sql`SELECT * FROM "State"`;

    const promises = Array(countRequests).fill(0).map((data, index) => {
      return limit(async () => {
        const start = performance.now();
        const data = await sql`SELECT * FROM "State"`;
        console.info(`Database request #${index} took ${performance.now() - start}ms (returned ${data.length} rows)`);
        return data;
      });
    });

    const start = performance.now();
    const promisesRes = await Promise.all(promises);
    console.info(`All database requests took ${performance.now() - start}ms`);

    res.write('OK');
  } else if (sql && req.url.startsWith('/postgres?index=')) {
    let i = 10;

    do {
      i--;
      const res = await sql`SELECT * FROM "State"`;
    } while (i > 0);

    res.write('OK');
  } else {
    // https://onlinefiletools.com/generate-random-text-file
    // Text of 1024 bytes
    res.write('nurrwqpepmxjascpzhptrhuhohswmctroxyhmxnrvhrnhlcfngavcgvomlcdyutpzewkhtdvbjncutazinnjfsseeblsvhpijnnoerqkbjwumvkhyqinjvglozlhhdpenjxklduhifcpkdaxonjnglympebaqfnmaflubtqsqjconqzywbvtskpgxxbmvgdwofbepuwugddjguzywnomfbiaxsvtzqdyobuzxgcgsiawpizkqnzarvdsxqmwrswmzrjzuvumpgngdgkasnbcsnqsmzpduhvdcffxgzfsbdmwghjemwtthvehcnwnehcfrathjnppookeppwjvfupxhoxtxeyqtnjlutmlzfxbsvjaostbacowxanpznhltllvikepswpkaprvmdxtfchwfaqcbnhictbegpxybppsuhmcpkxstkwqucwfbqvdktlaqeullnxjsynkpibzayzcbafjevtyyzlzbuyruadwaxawotblumdvmrxfbdbujcuinaxrerzixmqeqowqnjsjnpbmsbiznithkddjyyrajbzvzexiwdkmybxtcxpxxxrbwcqovfrxbfinfmhfkjscnlfqabbuzugczrbiqgiwckmwyjxapkmaaayyuxbpnrgxfaambpmilihqvrhgnedfgicenggindmqzqdymbxztxeeoovywjyavddubxmzcnbzpditdzvvkxziqffzyumttagsobtiwvylsblfjdepwanqxamlhamykjagfqkpcqwmwmhzfbntuuwhlujcwvppaqmwpvdqwqaaspskvdfesvzeykkpmxbntbepesielkruyabkdaabrcrxmgzfvpcjfsbqvzzrrlgwkzwvcbrkoeowfmijhygrjpwgwjjwieieocykikltqdibgtnwofhyfhlpgwbjkdrqxeebwbrqvrmaknlheinuhqjrnpkicozfxhlzljhwvhllsbqubefunlbhfdktvoiiqpsyqhswvtesckotdycetyhouhqnzcs');
  }
  res.end();

  console.info(`Request to ${req.url} ${performance.now() - start}ms`);
}).listen(8080); // Rhe server object listens on port 8080

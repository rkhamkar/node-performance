const countWarmupRequests = 0;

const countRequests = 500;
const concurrentRequests = 100;

// const endpoint = '';
const endpoint = 'postgres';

// const api = 'https://shark-app-edhvp.ondigitalocean.app/'; // DigitalOcean professional-xs
// const api = 'https://dolphin-app-hmkn4.ondigitalocean.app/'; // DigitalOcean professional-m
const api = 'http://157.90.19.212:8080/'; // hetzner CX21
// const api = 'http://localhost:8080/'; // localhost if you run node-server/index.js

async function job () {
  const pLimit = await import('p-limit');

  const warmupLimit = pLimit.default(100);
  const warmupPromises = Array(countWarmupRequests).fill(0).map((data, index) => {
    return warmupLimit(async () => {
      const response = await fetch(api + endpoint + '?warmup=' + index);
      return response.text();
    });
  });
  await Promise.all(warmupPromises);

  const limit = pLimit.default(concurrentRequests);

  const promises = Array(countRequests).fill(0).map((data, index) => {
    return limit(async () => {
      const start = performance.now();
      const response = await fetch(api + endpoint + '?index=' + index);
      const data = response.text();
      console.info(`Request #${index} took ${performance.now() - start}ms`);
      return data;
    });
  });

  const start = performance.now();
  const res = await Promise.all(promises);
  console.info(`All requests took ${performance.now() - start}ms`);
}

job().then(() => {
  console.info('Done');
}).catch((err) => {
  console.error(err)
});
